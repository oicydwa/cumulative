import java.util.Scanner;

public class Problem2 {

	public static void main(String[] args) {
		int numPrint=0;
		int index;
		int index2;
		
		Scanner keyboard = new Scanner(System.in);
		
		while(numPrint<=0 || numPrint>=21) {
			System.out.print("Enter the length(1-20): ");
			numPrint = keyboard.nextInt();
		}

		for(index = 0; index<numPrint; index++){
			for(index2 = 0; index2<numPrint; index2++) {
				System.out.print("* ");
			}
			System.out.print("\n");
		}
		keyboard.close();
	}

}
