import java.util.Scanner;

public class Problem2a {

	public static void main(String[] args) {
		int numPrint=0;
		int index;
		int index2;
		char ch;
		boolean isInt = true;
		String buffer = "";
		
		Scanner keyboard = new Scanner(System.in);
		
		do {
			System.out.print("Enter the length(1-20): ");
			buffer = keyboard.nextLine();
			
			for(index=0;index<buffer.length();index++) {
				ch = buffer.charAt(index);
				if (ch<'0' || ch>'9') {
					isInt = false;
					break;
				}
			}
			if(isInt&&buffer.length()>0) {
				numPrint=Integer.parseInt(buffer);
			}
			else {
				numPrint = 0;
			}
			isInt = true;
		}while(numPrint<=0 || numPrint>=21);

		for(index = 0; index<numPrint; index++){
			for(index2 = 0; index2<numPrint; index2++) {
				System.out.print("* ");
			}
			System.out.print("\n");
		}
		keyboard.close();
	}

}
