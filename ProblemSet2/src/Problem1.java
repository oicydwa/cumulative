import java.util.Scanner;

public class Problem1 {

	public static void main(String[] args) {
		int fNum;
		int sNum;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter first number: ");
		fNum = keyboard.nextInt();
		
		System.out.print("Enter second number: ");
		sNum = keyboard.nextInt();

		if(fNum<sNum) {
			System.out.printf("%d is less than %d",fNum, sNum);
		}else if(fNum>sNum) {
			System.out.printf("%d is greater than %d",fNum, sNum);
		}else if(fNum==sNum) {
			System.out.printf("%d is equal to %d",fNum, sNum);
		}
		
		keyboard.close();
	}

}