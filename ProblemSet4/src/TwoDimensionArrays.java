import java.util.Random;

public class TwoDimensionArrays {

	final static int ROWS = 3;
	final static int COLS = 4;
	
	public static void main(String[] args) {
		int[][] numbers = new int[ROWS][COLS];
		
		loadArray(numbers);
		
		displayArray(numbers);

	}
	
	public static void loadArray(int[][] theArray) {
		int rInd, cInd;
		Random rand = new Random();
		
		for(rInd=0;rInd<ROWS;rInd++) {
			for(cInd=0;cInd<COLS;cInd++) {
				theArray[rInd][cInd] = rand.nextInt(9)+1;
			}
		}
	}
	
	public static void displayArray(int theArray[][]) {
		int rInd, cInd;
		String line = "";
		for(int j =0; j<COLS;j++) {
			line += "-";
		}
		for(cInd=0;cInd<COLS;cInd++) {
			System.out.printf("[%d]", cInd);
		}
		System.out.println("\n"+line+line+line);
		for(rInd=0;rInd<ROWS;rInd++) {
			System.out.printf("[%d]", rInd);
			for(cInd=0;cInd<COLS;cInd++) {
				System.out.print(" "+theArray[rInd][cInd] + "|");
				
			}
			
			System.out.println("\n"+line+line+line);
		}
	}
}
