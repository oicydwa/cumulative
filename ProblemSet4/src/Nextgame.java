import java.util.Scanner;
import java.util.Random;

public class Nextgame {

	public static void main(String[] args) {
		int turnNum = 0;
		boolean answer = false;
		int[] solutionArray;
		int[] userGuess;
		int numCorrect=0;
		
		displayRules();
		solutionArray= createSolutionArray();
		
//		for(index=0;index<solutionArray.length;index++) {
//			System.out.printf("%d ", solutionArray[index]);
//		}System.out.println("");
		
		while (!answer){
			turnNum++;
			userGuess = getUserGuess(turnNum);
			numCorrect = compareGuess(userGuess,solutionArray);
			if(numCorrect == -1) {
				break;
			}
			if(numCorrect == 5) {
				System.out.printf("You guessed the sequence in %d turns ", turnNum);
				break;
			}
			System.out.printf("You have %d numbers correct\n",numCorrect);
			
		}
		displaySolution(solutionArray);

	}
	public static void displaySolution(int[] solution) {
		
	}
	
	public static void displayRules() {
		System.out.println("Game: Who's Next Objective: Identify the Sequence of 5 numbers between 1 and 5 using the fewest \n"
				+ "turns. If you wish to quit guessing and give up, enter a ZERO for one of your \n"
				+ "guesses and the game will display the solution and quit.\n"
				+ "GOOD LUCK!!! ");
	}
	
	public static int[] createSolutionArray() {
		Random rand = new Random();
		int index;
		int loc1;
		int loc2;
		int temp;
		int[] numbers = {1,2,3,4,5};
		for(index=0;index<numbers.length;index++) {
			loc1 = rand.nextInt(5);
			loc2 = rand.nextInt(5);
			temp = numbers[loc1];
			numbers[loc1] = numbers[loc2];
			numbers[loc2] = temp;
		}
		
		return numbers;
	}
	
	@SuppressWarnings("resource")
	public static int[] getUserGuess(int turnNum) {
		String guess;
		int[] formattedGuess;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.printf("== Turn %d == Number Sequence: ", turnNum);
		guess = keyboard.nextLine();
		formattedGuess = split(guess);
		
		return formattedGuess;
	}
	
	public static int compareGuess(int[] guess, int[] answer) {
		int index;
		int numCorrect=0;
		for (index=0;index<guess.length;index++) {
			if(guess[index]==0) {
				numCorrect = -1;
				break;
			}
			if (guess[index] == answer[index])
				numCorrect++;
		}
		
		return numCorrect;
	}
	
	public static int[] split(String str) {
		int[] listed;
		int index; 
		char ch = ' ';
		
		listed = new int[5]; 

		for (index = 0; index < str.length(); index++) {
			ch = str.charAt(index);
			if (ch == '0' || ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5') {
				listed[index] = ch-48;
			}
		}	
		return listed;
	}

}
