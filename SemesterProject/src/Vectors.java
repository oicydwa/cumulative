import java.util.Scanner;
import java.util.Random;

/**
 * 
 * @author Chris shipley
 *
 */ 
public class Vectors {
	
	public static final int ROWS = 10; //set number of rows
	public static final int COLS = 10; //set number of columns
	
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		char[][] grid = new char[ROWS][COLS]; // create our 10x10 grid
		
		int[] scores = new int[8]; //this is to simplify sending our data to our grid
		/**
		 * scores[0] = player 1 current round score
		 * scores[1] = player 1 last round score
		 * scores[2] = player 2 current round score
		 * scores[3] = player 2 last round score
		 * scores[4] = current round
		 * scores[5] = total rounds
		 * scores[6] = player 1 total score
		 * scores[7] = player 2 total score
		 */
		int rounds;
		int rnd = 1;

		zeroScores(scores); //create all 0's in our scores array
		initializeGrid(grid); //fill our grid with blank spaces
		
		
		rounds = 2; //howManyRnds(keyboard);
		
		scores [5] = rounds;
		
		while(rnd <= rounds) {
			scores[4] = rnd; //assign our rnd to our array holder
			computerInput(grid); //get the computer's move
			
			if(rnd ==1) {
				displayGrid(grid, scores); //show our game board
			}
			inputTestData(grid);
			userInput(grid, keyboard); //get user input
			
			checkScore(grid, scores);
			
			displayGrid(grid, scores); //show our game board
			
			rnd++;
		}
		System.out.printf("\n\n");
		if(scores[6] > scores[7]) {
			System.out.println("|+++++++++++++++++++++++++++++++|");
			System.out.printf("|*  Final Scores P1:%3d P2:%3d *|\n",scores[6],scores[7]);
			System.out.println("|*******************************|");
			System.out.println("|*Congratulations Player One!!!*|");
			System.out.println("|*******************************|");
			System.out.println("|+++++++++++++++++++++++++++++++|");
		}else if(scores[7] > scores[6]) {
			System.out.println("|+++++++++++++++++++++++++++++++|");
			System.out.printf("|*  Final Scores P1:%3d P2:%3d *|\n",scores[6],scores[7]);
			System.out.println("|*******************************|");
			System.out.println("|*Congratulations Player Two!!!*|");
			System.out.println("|*******************************|");
			System.out.println("|+++++++++++++++++++++++++++++++|");
		}else {
			System.out.println("|+++++++++++++++++++++++++++++++|");
			System.out.printf("|*  Final Scores P1:%3d P2:%3d *|\n",scores[6],scores[7]);
			System.out.println("|*******************************|");
			System.out.println("|*       We have a tie!!       *|");
			System.out.println("|*******************************|");
			System.out.println("|+++++++++++++++++++++++++++++++|");
		}
		
		keyboard.close(); //uh... duh!
	}
	
	public static char[][] inputTestData(char[][] grid){
		
		grid[0][9] = '1';
		grid[1][8] = '1';
		grid[2][7] = '1';
		grid[3][6] = '1';
		grid[4][5] = '1';
		grid[5][4] = '1';
		
		
		return grid;
	}
	
	/**
	 * 
	 * @param grid
	 * @param scores
	 * @return
	 */
	public static int[] checkScore(char[][] grid, int[] scores){
		int rI;
		int cI;
		char player;
		
		double test = 0.0;
		double test2 = 0.0;
		
		scores[1] = scores[0];
		scores[3] = scores[2];
		
		for(rI=0;rI<ROWS;rI++) {
			for(cI=0;cI<COLS;cI++) {
				if(grid[rI][cI] != ' ' && grid[rI][cI] != '*') {
					player = grid[rI][cI];
					if(grid[rI][cI] != ' ' && grid[rI][cI] != '*') {
						if(player == '1') {   //char[][] grid, int curRow, int curCol, char player
							test += checkAdjacent(grid,rI,cI,'1');
						}else if(player == '2') {
							test2 += checkAdjacent(grid,rI,cI,'2');
						}
					}
				}
			}
		}
		if((int)test == 4) {
			test = 5;
		}else if ((int)test >= 5) {
			test= 8;
		}
		scores[0] = (int)test;
		
		if((int)test2 == 4) {
			test2 = 5;
		}else if ((int)test2 >= 5) {
			test2= 8;
		}
		scores[2] = (int)test2;
		
		scores[6] += scores[0];
		scores[7] += scores[2];
		
		return scores;
	}
	
	/**
	 * 
	 * @param grid
	 * @param curRow
	 * @param curCol
	 * @param player
	 * @return
	 */
	public static double checkAdjacent(char[][] grid, int curRow, int curCol, char player) {
		int[][] where = new int[8][2];
		int found = 0;
		int[][] chase = new int[8][2];
		double totScore = 0;
		
		/**
		 * this next codeblock is formatted weird for space
		 * basically it creates an array for each direction that uses the current x y coord
		 * and subtracts or adds to make it move in a fixed direction from the current point
		 * added these to an array, firstly for a test of concept, and secondly to condense
		 * the lines of code later. Should be fairly self-explanatory when read with this in mind
		 */
		int[] upLeft = new int[2]; 
			upLeft[0] = curRow-1; 
			upLeft[1] = curCol-1; 
			where[0] = upLeft;
		int[] up = new int[2]; 
			up[0] = curRow-1; 
			up[1] = curCol; 
			where[1] = up;
		int[] upRight = new int[2]; 
			upRight[0] = curRow-1; 
			upRight[1] = curCol+1; 
			where[2] = upRight;
		int[] right = new int[2]; 
			right[0] = curRow; 
			right[1] = curCol+1; 
			where[3] = right;
		int[] downRight = new int[2]; 
			downRight[0] = curRow+1; 
			downRight[1] = curCol+1; 
			where[4] = downRight;
		int[] down = new int[2]; 
			down[0] = curRow+1; 
			down[1] = curCol; 
			where[5] = down;
		int[] downLeft = new int[2]; 
			downLeft[0] = curRow+1; 
			downLeft[1] = curCol-1; 
			where[6] = downLeft;
		int[] left = new int[2]; 
			left[0] = curRow; 
			left[1] = curCol-1; 
			where[7] = left;
		
		
			
		if(curRow != 0 && curRow != 9 && curCol != 0 && curCol != 9) {
			for(int x=0; x<8; x++) { //this is a simple way to test all 8 locations around 
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
		}else if(curRow == 0 && curCol != 9 && curCol !=0 ){
			//test everything but up(s)
			for(int x=3; x<8; x++) {
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
		}else if(curRow == 9 && curCol != 9 && curCol !=0) {
			//test everything but down(s)
			for(int x=0; x<4; x++) {
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
			if(grid[where[7][0]][where[7][1]] == player) {
				
				chase[found][0] = where[7][0];
				chase[found][1]	= where[7][1];
				found++;
			}
		}else if(curCol == 0 && curRow != 9 && curRow !=0) {
			//test everything but left(s)
			for(int x=1; x<6; x++) {
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
		}else if(curCol == 9 && curRow != 9 && curRow !=0) {
			//test everything but right(s)
			for(int x=0; x<2; x++) {
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
			for(int x=5; x<8; x++) {
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
		}else if(curRow == 0 && curCol == 0) {
			//no up or left
			for(int x=3; x<6; x++) {
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
		}else if(curRow == 0 && curCol == 9) {
			//no up or right
			for(int x=5; x<8; x++) {
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
		}else if(curRow == 9 && curCol == 0) {
			//no down or left
			for(int x=1; x<4; x++) {
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
		}else if(curRow == 9 && curCol == 9) {
			//no down or right
			for(int x=0; x<1; x++) {
				if(grid[where[x][0]][where[x][1]] == player) {
					
					chase[found][0] = where[x][0];
					chase[found][1]	= where[x][1];
					found++;
				}
			}
			if(grid[where[7][0]][where[7][1]] == player) {
				
				chase[found][0] = where[7][0];
				chase[found][1]	= where[7][1];
				found++;
			}
		}
				
		totScore = downTheRabbitHole(grid, chase, found, curRow, curCol, player);
		
		return totScore;
	}
	
    /**
     * 
     * @param grid
     * @param chase
     * @param len
     * @param curRow
     * @param curCol
     * @param player
     * @return
     */
    public static double downTheRabbitHole(char[][] grid, int[][] chase, int len, int curRow, int curCol, char player) {
    	int index;
    	int row;
    	int col;
    	int numMatches = 0;
    	int total;
    	double totScore = 0;
    	int[] numberMatches = new int[8];
    	double divisor;
    	double tally;
    	
    	
    	for(index=0;index<len;index++) {
    		row = chase[index][0];
    		col = chase[index][1];
    		
    		if(row-curRow == -1 && col-curCol == -1 && curRow != 0 && curCol !=0) {// direction is up and left
    			numMatches = testDirection(grid, player, -1, -1, row, col);
    			numberMatches[0] = numMatches;
    			
    		}else if(row-curRow==-1 && col-curCol==0) {// direction is straight up
    			numMatches = testDirection(grid, player, -1, 0, row, col);
    			numberMatches[1] = numMatches;
    			
    		}else if(row-curRow==-1 && col-curCol==1) { //direction is up and right
    			numMatches = testDirection(grid, player, -1, 1, row, col);
    			numberMatches[2] = numMatches;
    			
    		}else if(row-curRow==0 && col-curCol==1) { //direction is directly right
    			numMatches = testDirection(grid, player, 0, 1, row, col);
    			numberMatches[3] = numMatches;
    			
    		}else if(row-curRow==1 && col-curCol==1) { // direction is right and down
    			numMatches = testDirection(grid, player, 1, 1, row, col);
    			numberMatches[4] = numMatches;
    			
    		}else if(row-curRow==1 && col-curCol==0) {//direction is straight down
    			numMatches = testDirection(grid, player, 1, 0, row, col);
    			numberMatches[5] = numMatches;
    			
    		}else if(row-curRow==1 && col-curCol==-1) {//direction is left and down
    			numMatches = testDirection(grid, player, 1, -1, row, col);
    			numberMatches[6] = numMatches;
    			
    		}else if(row-curRow==0 && col-curCol==-1) {//direction is directly left
    			numMatches = testDirection(grid, player, 0, -1, row, col);
    			numberMatches[7] = numMatches;
    			
    		}
    	}
    	
    	
    	total = numberMatches[0] + numberMatches[4];
		if(total >= 3) {
			divisor = 1/(double)total;
			tally = 1+divisor;
			totScore+=tally;
		}
		total = numberMatches[1] + numberMatches[5];
		if(total >= 3) {
			divisor = 1/(double)total;
			tally = 1+divisor;
			totScore+=tally;
		}
		total = numberMatches[2] + numberMatches[6];
		if(total >= 3) {
			divisor = 1/(double)total;
			tally = 1+divisor;
			totScore+=tally;
		}
		total = numberMatches[3] + numberMatches[7];
		if(total >= 3) {
			divisor = 1/(double)total;
			tally = 1+divisor;
			totScore+=tally;
		}
		
    	return totScore;
    }
    
    /**
     * 
     * @param grid
     * @param player
     * @param rDirection
     * @param cDirection
     * @param xCoord
     * @param yCoord
     * @return
     */
    public static int testDirection(char[][] grid, char player, int rDirection, int cDirection, int xCoord, int yCoord) {
    	int matches = 0;
    	char charAtLoc = player;
    	
    	while(charAtLoc == player) {
    		if(xCoord != -1 && yCoord != -1 && xCoord != 10 && yCoord != 10){
    			charAtLoc = grid[xCoord][yCoord];
        		xCoord += rDirection;
        		yCoord += cDirection;
        		matches++;
    		}else {
    			matches++;
    			break;
    		}
    		
    	}
    	
    	return matches;
    }
	
	/**
	 * get the number of rounds. with input validation
	 * @param keyboard
	 * @return
	 */
    public static int howManyRnds(Scanner keyboard) {
        int rounds = -1; //initialize to something invalid
        while(rounds<1 || rounds >25) { //as long as our rounds are between 1 and 25 continue
            boolean condition = false; //need a bool to keep looping until we receive valid input
            while(!condition) { //see one line up
            	System.out.print("How many rounds would you like to play?\n(Hit enter for default 25):");
                String test = keyboard.nextLine();
                if(validateRndsInput(test)) //use method to validate our input is within our parameters
	            	if(test.equals("")) { //if user just hits enter....
	                    rounds = 25; // .....set default rounds to 25
	                    condition = true; //to escape our loop we are good so set to true
	                }else if(Integer.parseInt(test) >0 && Integer.parseInt(test)<25){ //as long as our number is between 0 and 25
	                    rounds = Integer.parseInt(test); //set our rounds to the number input by the user
	                    condition = true; //to escape our loop we are good so set to true
	                }
            	}
            }    
        System.out.println(""); //need a line between input of lines and start of grid output
        return rounds;
            
    }
    
    /**
     * input validation for number of rounds input by the user.    
     * @param str
     * @return
     */
    public static boolean validateRndsInput(String str) {
    	boolean isGood = true;
    	int index;
    	char ch;
    	for(index=0;index<str.length();index++) { 
    		ch = str.charAt(index); 
    		if(ch <= 48 || ch >= 57) { // if our character being tested is anything outside the numbers 0 through 9...
    			isGood = false; // ...we say that it's not a valid input and ...
    			break; // ... break out of our loop, because we need no further validation
    		}
    	}
    	return isGood;
    }
	
	/**
	 * initialize the board with blank spaces
	 * @param grid
	 * @return
	 */
	public static char[][] initializeGrid(char[][] grid){
		int rI; //index for rows
		int cI; //index for columns
		// iterate through 2 dimensional array and fill all slots with ' '
		for(rI=0;rI<ROWS;rI++) {
			for(cI=0;cI<COLS;cI++) {
				grid[rI][cI] = ' ';
			}
		}
		
		return grid; //return our zeroed out grid
	}
	
	/**
	 * zeroes out all the array items for my score elements
	 * @param scores
	 * @return
	 */
	public static int[] zeroScores(int[] scores) {
		int index; 
		for(index=0;index<scores.length;index++) {
			scores[index] = 0;
		}
		
		return scores;
	}
	
	/**
	 * computer's input
	 * simply randomizes two variables from 0 to 9 and inserts '*' at 
	 * 		that location in our grid
	 * 
	 * Has to return our modified array...
	 * @param grid
	 * @return
	 */
	public static char[][] computerInput(char[][] grid) {
		Random rand = new Random();
		int x = 0;
		int y = 0;
		boolean good = false;
		
		while(!good) {
			x = rand.nextInt(10); //our x is our Rows (x) coordinate
			y = rand.nextInt(10); //our y is our Columns (y) coordinate
			
			if( grid[x][y] != '*') {
				if(grid[x][y] != ' ') {
					checkAdjacent(grid,x,y,grid[x][y]);
				}
				grid[x][y] = '*';
				good = true;
			} //insert our computers move (*) at our random coordinate
		}
		
		return grid; //return our modified array
	}
	
	/**
	 * display board and points information and prompt for user input
	 * @param grid 
	 * @param scores
	 */
	public static void displayGrid(char[][] grid, int[] scores) {
		/** can pass an array with characters scores and such in it...
		 * scores[] 0 = p1RndScore 1 = p1LstRnd
		 * 		2 = p2RndScore 3 = p2LstRnd
		 * 		4 = curRnd 
		 */
		String bar = "*******************************************";
		int p1RndScore = scores[0];
		int p1LstRnd = scores[1];
		int p2RndScore = scores[2];
		int p2LstRnd = scores[3];
		int curRnd = scores[4];
		int totRnds = scores[5];
		int p1Total = scores[6];
		int p2Total = scores[7];
		
		System.out.println(bar);
		System.out.println("*             V E C T O R S               *");
		System.out.println(bar);
		System.out.printf("* Player One Score:   %2d   Last Round  %2d *%n", p1RndScore, p1LstRnd );
		System.out.printf("* Player Two Score:   %2d   Last Round  %2d *%n", p2RndScore, p2LstRnd);
		System.out.printf("* Round  %d  of %2d                         *%n", curRnd, totRnds);
		System.out.printf("*    Player 1:%3d  Player 2:%3d           *%n", p1Total, p2Total);
		System.out.println(bar);
		displayGridArray(grid);
	}
	
	/**
	 * uses for loop to display current data in the grid
	 * @param grid
	 */
	public static void displayGridArray(char[][] grid) {
		int rI;
		int cI;
		
		for(cI=0;cI<COLS;cI++) {
			System.out.printf("  %d ", cI);
		}
		System.out.println("\n-----------------------------------------");
		for(rI=0;rI<ROWS;rI++) {
			for(cI=0;cI<COLS;cI++) {
				System.out.printf("%2c |", grid[rI][cI]);
			}
			System.out.printf("%d%n", rI);
			System.out.println("-----------------------------------------");
		}
	}
	
	/**
	 * get user input and validate it
	 * take Scanner input, array information (for validation)
	 * 
	 * @param grid 
	 * @param keyboard
	 * @return
	 */
	public static char[][] userInput(char[][] grid, Scanner keyboard){
		boolean condition = false;
		boolean condition2 = false;
		String buffer;
		String[] temp;
		int[] input = new int[2];
		
		while(!condition) { //all this only sets player 1, need a copy for player 2
			System.out.print("Player 1 move (Row Col): ");
			buffer = keyboard.nextLine();
			if(buffer.length() == 3) {
				if((buffer.charAt(0) >= 48 && buffer.charAt(0) <= 57) && buffer.charAt(1) == ' ' && (buffer.charAt(2) >= 48 && buffer.charAt(2) <= 57)) {
					temp = buffer.split(" "); //split buffer into two separate pieces
					input[0] = Integer.parseInt(temp[0]); //cast to integer
					input[1] = Integer.parseInt(temp[1]); //ditto
					if(validateTarget(grid, input[0], input[1])){
						grid[input[0]][input[1]] = '1'; 
						condition = true;
					}
				}
			}
		}
		while(!condition2) { //this is identical to player 1, but for player two. Only difference is a second condition for second while loop.
			System.out.print("Player 2 move (Row Col): ");
			buffer = keyboard.nextLine();
			if(buffer.length() == 3) {
				if((buffer.charAt(0) >= 48 && buffer.charAt(0) <= 57) && buffer.charAt(1) == ' ' && (buffer.charAt(2) >= 48 && buffer.charAt(2) <= 57)) {
					temp = buffer.split(" "); //split buffer into two separate pieces
					input[0] = Integer.parseInt(temp[0]); //cast to integer
					input[1] = Integer.parseInt(temp[1]); //ditto
					if(validateTarget(grid, input[0], input[1])){
						grid[input[0]][input[1]] = '2'; 
						condition2 = true;
					}
				}
			}
		}
		
		return grid;
	}
	
	/**
	 * test if the location chosen is available. return "true" if it's not.
	 * @param grid
	 * @param x
	 * @param y
	 * @return
	 */
	public static boolean validateTarget(char[][] grid, int x, int y) {
		boolean result = true;
		
		if(grid[x][y] != ' ') {
			result = false;
		}
		
		return result;
	}
	
}

