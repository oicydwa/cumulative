
public class Problem1 {

	public static void main(String[] args) {
		int[] binary = {1,0,1,0,1,0,0};
		int parityTest;
		
		parityTest = parity(binary);
		
		System.out.println("Result is "+parityTest);
	}
	public static int parity(int[] binary) {
		int result = 9;
		int index;
		int count0=0;
		int count1=0;
		
		for(index=0;index<binary.length;index++) {
			if(binary[index] == 0) {
				count0++;
			}else if(binary[index] == 1){
				count1++;
			}
		}
		if(count0>count1) {
			result = 0;
		}else if(count1>count0) {
			result = 1;
		}else if(count1==count0){
			result = -1;
		}
		
		return result;
	}
}
