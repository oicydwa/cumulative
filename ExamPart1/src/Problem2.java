
public class Problem2 {

	public static void main(String[] args) {
		int[] numbers = {1,1,3,2,1,1,1,1,5,3,3,3,7,7,7,7,7,7,2,2};
		int max;
		
		max = maxSequence(numbers);
		
		System.out.println("Max consecutive numbers is "+max);
	}
	public static int maxSequence(int[] numbers) {
		int max = 1;
		int index;
		int num = numbers[0];
		int preMax=0;
		
		for(index=0;index<numbers.length;index++) {
			if(num == numbers[index] && index!=0) {
				max++;
			}
			if(num != numbers[index]&&index!=0){
				if(preMax < max) {
					preMax = max;
					max = 1;
				}else if(preMax>max){
					max = 1;
				}
				
			}
			
			num = numbers[index];
		}
		
		//1,1,3,2,1,1,1,1,5,3,3,3,6,7,7,7,7,7,2,2
		
		return preMax;
	}
}
