
public class Problem2 {

	public static void main(String[] args){
	   int[] array1={1,2,2,3,4,5,5,3,7,7};
	   int[] array2={1,2,3,4,5,6,7,1};
	   int[] array3={1,2,3,3,2,1};
	   int[] array4={1,1,1,2,3,4,5};

	   displayArray(array1,"Array");
	   System.out.printf("Correct Return Value 3 ---> Your Function Return Value %d\n\n",countPairs(array1));

	   displayArray(array2,"Array");
	   System.out.printf("Correct Return Value 0 ---> Your Function Return Value %d\n\n",countPairs(array2));

	   displayArray(array3,"Array");
	   System.out.printf("Correct Return Value 1 ---> Your Function Return Value %d\n\n",countPairs(array3));

	   displayArray(array4,"Array");
	   System.out.printf("Correct Return Value 2 ---> Your Function Return Value %d\n\n",countPairs(array4));
	}

	public static void displayArray(int array[], String str){
	   int index;

	   System.out.printf("%s: { ",str);
	   for(index=0;index<array.length;index++){
	      if(index!=0) System.out.printf(",");
	      System.out.printf("%d",array[index]);
	   }
	   System.out.printf(" }\n");

	}
	//**************** DO NOT MODIFY ANY OF THIS PROGRAM ABOVE THIS LINE ******************************
	//**************** It Contains the code to test your function        ******************************
	//**************** Put your code in the function, compile, and run the program ********************
	//**************** to see if Return Value is Correct                           ********************

	public static int countPairs(int array[]){
		int index;
		int total =0;
		
		for(index=0;index<array.length-1;index++) {
			if(array[index]==array[index+1]) {
				total++;
			}
		}
		
	   return total;
	}

}
