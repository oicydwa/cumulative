
public class Problem1 {

	public static void main(String[] args){
	   int[] array1={1,2,5,3,4,5,5,3,7,7};
	   int[] array2={1,2,3,4,5,6,7,1};
	   int[] array3={2,4,6};
	   int[] array4={1,1,1,2,3,4,5};

	   displayArray(array1,"Array");
	   System.out.printf("Correct Return Value 36 ---> Your Function Return Value %d\n\n",sumOdds(array1));

	   displayArray(array2,"Array");
	   System.out.printf("Correct Return Value 17 ---> Your Function Return Value %d\n\n",sumOdds(array2));

	   displayArray(array3,"Array");
	   System.out.printf("Correct Return Value 0 ---> Your Function Return Value %d\n\n",sumOdds(array3));

	   displayArray(array4,"Array");
	   System.out.printf("Correct Return Value 11 ---> Your Function Return Value %d\n\n",sumOdds(array4));
	}

	public static void displayArray(int array[], String str){
	   int index;

	   System.out.printf("%s: { ",str);
	   for(index=0;index<array.length;index++){
	      if(index!=0) System.out.printf(",");
	      System.out.printf("%d",array[index]);
	   }
	   System.out.printf(" }\n");

	}
	//**************** DO NOT MODIFY ANY OF THIS PROGRAM ABOVE THIS LINE ******************************
	//**************** It Contains the code to test your function        ******************************
	//**************** Put your code in the function, compile, and run the program ********************
	//**************** to see if Return Value is Correct                           ********************

	public static int sumOdds(int array[]){
		int total=0;
		int index;
		
		for(index=0;index<array.length;index++) {
			if(array[index]%2 != 0) {
				total += array[index];
			}
		}
		
	   return total;
	}

}
