import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Analyze {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner keyboard = new Scanner(System.in);
		
		//System.out.print("What is the filename?");
		String filename = "declaration.txt"; //keyboard.nextLine();
		
		File file = new File(filename);
		Scanner in = new Scanner(file);
		int line = 0;
		String[] document = new String[100000];
		double percentage;
		int index;
		int ind;
		char perc = 37;
		String lineInArray = "";
		int finalCount = 0;
		char ch = ' ';
		int location = -1;
		char[] alpha = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		int[] counts = new int[26];
		
		
		while(in.hasNextLine()) {
			document[line] = in.nextLine();
			line++;
		}
		
		for(index = 0; index < counts.length; index++) {
			counts[index] = 0;
		}
		
		for(index = 0; index< line; index++) {
			lineInArray = document[index];
			for(ind =0; ind<lineInArray.length(); ind++) {
				ch = lineInArray.charAt(ind);
				if(ch >= 65 && ch<= 90) {
					location =ch -65;
					counts[location] = counts[location]+1;
					finalCount++;
					
				}else if(ch>= 97 && ch<= 122) {
					location= ch -97;
					counts[location] = counts[location]+1;
					finalCount++;
					
				}
				
			}
		}
		
		System.out.printf("Filename:\t%s\nLetter Count: \t%d\n-------------------------------\n\n",filename,finalCount);
		for(index = 0; index < alpha.length; index++) {
			percentage = ((double)counts[index]/(double)finalCount)*100;
			System.out.printf("%c:\t%4d\t[%6.3f%c]\n",alpha[index], counts[index],percentage,perc);
		}
		in.close();
		keyboard.close();
	}

}