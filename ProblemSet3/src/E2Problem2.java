import java.util.Scanner;

public class E2Problem2 {

	public static void main(String[] args) {
		String str;
		int digitsSum;
		
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter a string: ");
		str = keyboard.nextLine();
		
		digitsSum = countDigits(str);
		
		
		System.out.printf("Sum of Digits: %d",digitsSum);
		
		keyboard.close();
	}

	public static int countDigits(String str) {
		int index;
		char ch = ' ';
		int[] intArray;
		int arrayIndex = 0;
		int intCount = 0;
		int intSum = 0;
		
		for(index =0; index < str.length(); index++) {
			ch = str.charAt(index);
			if(ch >= 48 && ch <= 57) {
				intCount++;
			}
		}
		System.out.printf("\nDigit Count: %d\n", intCount);
		intArray = new int[intCount];
		for(index =0; index < str.length(); index++) {
			ch = str.charAt(index);
			if(ch >= 48 && ch <= 57) {
				intArray[arrayIndex] = ch;
				arrayIndex++;
			}
		}
		for(index = 0; index < intArray.length; index++) {
			intSum += intArray[index]-48;
		}
		
		return intSum;
	}
	
}
