import java.util.Random;

public class ProblemOne {

	public static void main(String[] args) {
		int[] values = new int[20];
		int index;
		int smallest;
		
		
		for(index = 0; index < 20; index++) {
			values[index] = randNum();
		}
		for(index = 0; index < values.length; index++) {
			System.out.printf("values[%2d]: %3d\n", index,values[index]);
		}
		
		smallest = indexOfSmallestNumber(values);
		System.out.println("\nIndex of Smallest Value: " +smallest);
	}
	public static int randNum() {
		Random rand = new Random();
		
		int val = rand.nextInt(499)+1;
		
		return val;
	}
	
	public static int indexOfSmallestNumber(int[] theArray) {
		int smallest = theArray[0];
		int index;
		int lowestNumber=0;
				
		for (index = 0; index < theArray.length; index++) {
			
			if (smallest > theArray[index]) {
				smallest = theArray[index];
				lowestNumber = index;
			}
		}		
		return lowestNumber;
	}
	
//	public static int findLocation(int[] anArray, int varToFind) {
//		int index;
//		int found = 0;
//		
//		for (index = 0; index < anArray.length; index++) {
//			if (anArray[index] == varToFind) {
//				found = index;
//			}
//		}
//		return found;
//	}

}
