import java.util.Scanner;

public class E2Problem1 {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int startNum;
		
		System.out.print("Enter starting number: ");
		startNum = keyboard.nextInt();
		
		outputCount(startNum);
		
		
		keyboard.close();
	}
	public static void outputCount(int start) {
		int index;
		int topNum = start+25;
		System.out.print("<");
		for(index = start; index < topNum; index ++) {
			
			if(index%5 != 0) {
				System.out.print(" -");
			}else {
				System.out.print(" " +index);
			}
			
		}
		System.out.print(" >");
	}
}
