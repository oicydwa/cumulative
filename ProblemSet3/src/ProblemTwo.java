
public class ProblemTwo {

	public static void main(String[] args) {
		String str = "apples,bananas,pears,oranges";
		String[] fruit;
		int index;
		
		fruit = split(str, ',');
		
		for(index = 0; index < fruit.length; index++) {
			System.out.println(fruit[index]);
		}
	}
	
	public static String[] split(String str, char delimiter) {
		String[] listed;
		int index; 
		char ch = ' ';
		int wordCount = 0;
		String word = "";
		int ind2 = 0;	
		
		wordCount = getWordCount(str, delimiter);
		//create array with length equal to word count
		listed = new String[wordCount]; 

		//Split sentence into array by delimiter
		for (index = 0; index < str.length(); index++) {
			ch = str.charAt(index);
			if (ch != delimiter) {
				word += ch;
				if(index+1 == str.length()) {
				listed[ind2] = word;
				}
			}else {
				listed[ind2] = word;
				word = "";
				ind2++;
			}
		}	
		return listed;
	}
	public static int getWordCount(String str, char delimiter) {
		int index; 
		char ch = ' ';
		int wordCount = 0;
		char preChar = ' ';
		
		//Get word count
		for(index = 0; index < str.length(); index++) {
			ch = str.charAt(index);
			if (ch == delimiter && preChar != delimiter) {
				wordCount++;
				//System.out.printf("wordCount is %d\n", wordCount);
			}
			preChar = ch;
		}
		if (preChar != delimiter) {
			wordCount++;
			//System.out.printf("wordCount is %d\n", wordCount);
		}
		return wordCount;
	}
}
