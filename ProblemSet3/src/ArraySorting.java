import java.util.Scanner;
import java.util.Random;

public class ArraySorting {

	public static void main(String[] args) {
		int[] numbers = new int[100000];
		//int[] numbers = {3,4,2,5,1};
		
		Scanner keyboard = new Scanner(System.in);
		
		loadArray(numbers);
		sortArray(numbers);
		displayArray(numbers);
		//System.out.print("Enter the search value: ");
		//value = keyboard.nextInt();
		//location = searchArray(numbers,value);
		
		
		
		//System.out.printf("%d is located at index %d. ",value, location);
		
		keyboard.close();
	}
	
	public static void displayArray(int[] theArray) {
		int index;
		
		for(index=0;index<theArray.length;index++) {
			System.out.printf("Array[%2d]: %3d\n",index, theArray[index]);
		}
	}
	
	public static int searchArray(int[] theArray, int value) {
		int index;
		int location = -1;
		
		for(index=0;index<theArray.length;index++) {
			if (theArray[index]==value) {
				location = index;
				break;
			}
		}
		
		return location;
	}
	
	public static void loadArray(int[] theArray) {
		Random rand = new Random();
		int index;
		
		for(index=0;index<theArray.length;index++) {
			theArray[index] = rand.nextInt(1000);
		}
	}
	
	public static void sortArray(int[] theArray) {
		int index;
		int pass;
		int temp;
		
		for(pass=0;pass<theArray.length-1;pass++) {
			for(index=0;index<theArray.length-1;index++) {
				if(theArray[index]>theArray[index+1]) {
					temp = theArray[index];
					theArray[index] = theArray[index+1];
					theArray[index+1] = temp;
				}
			}
		}
	}
}
