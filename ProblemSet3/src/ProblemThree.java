
public class ProblemThree {

	public static void main(String[] args) {
		String str1 = "smitz";
		String str2 = "smitz";
		int result;
		
		result=strCompare(str1,str2);

		System.out.printf("result is %d", result);
		
	}
	
	public static int strCompare(String stringA, String stringB) {
		int comparitor = 0;
		int index = 0;
		char char1 = ' ';
		char char2 = ' ';
		String shorter = "";
		
		if(stringA.length() > stringB.length()) {
			shorter = stringB;
			comparitor = 1;
		}else if(stringA.length() < stringB.length()) {
			shorter = stringA;
			comparitor = -1;
		}else {
			comparitor = 0;
		}
		
		while (index < shorter.length()) {
			char1 = stringA.charAt(index);
			char2 = stringB.charAt(index);
			if (char1 > char2) {
				//System.out.printf("%c is bigger than %c\n", char1, char2);
				comparitor = 1;
				break;
			}else if (char1 < char2) {
				//System.out.printf("%c is smaller than %c\n", char1, char2);
				comparitor = -1;
				break;
			}else {
				comparitor = 0;
				index++;
			}
			
		}
		return comparitor;
	}
}
