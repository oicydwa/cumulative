import java.util.Scanner;

public class Equation {

	public static void main(String[] args) {
		int result, int1, int2;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("For the equation '3x + 4z + 1' \nInput value for x: ");
		int1 = keyboard.nextInt();
		System.out.print("Input value for z: ");
		int2 = keyboard.nextInt();
		
		result = equation(int1,int2);
		
		System.out.print(result);
		
		keyboard.close();
	}
	
	public static int equation(int x, int z) {
		int solution;
		
		solution = 3*x+4*z+1;
		
		return solution;
	}

}
