import java.util.Scanner;

public class CountWords {

	public static void main(String[] args) {
		String sentence;
		int count;
		
		sentence = getSentence();
		count = wordCount(sentence);
		System.out.printf("There are %d words in the sentence '%s'.", count, sentence);
	}
	
	public static String getSentence() {
		String sentence;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Input a sentence: ");
		sentence = keyboard.nextLine();
		
		keyboard.close();
		return sentence;
	}
	
	public static int wordCount(String str) {
		char ch;
		int index;
		int wdCount = 0;
		char preChar = ' ';
		
		for(index = 0; index < str.length(); index ++) {
			ch = str.charAt(index);
			if (ch == ' ' && preChar != ' ') {
				wdCount++;
			}
			preChar = ch;
		}
		if (preChar != ' ') {
			wdCount++;
		}
		//wdCount++;
		return wdCount;
	}

}
