
public class ArrayOne {

	public static void main(String[] args) {
		int[] values = {9,5,6,3,7,4,3,2,8};
		int index;
		int total = 0;
		
		for (index=0;index<values.length;index++) {
			total += values[index];
			System.out.printf("values[%d]: %d\n", index, values[index]);
			System.out.printf("      total is %d\n", total);
		}
	}

}
