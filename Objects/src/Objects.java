import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Objects {

	public static void main(String[] args) throws FileNotFoundException {
		final int SIZE = 100;
		Record[] records = new Record[SIZE];
		int noRecords;
		
		Scanner keyboard = new Scanner(System.in);
		
		//System.out.print("What is the filename?");
		String filename = "records.csv"; //keyboard.nextLine();
		
		noRecords = readFile(filename, records);
		displayRecords(records,noRecords);
//		for(index=0;index<noRecords;index++) {
//			
//		}
		
		keyboard.close();
	}
	public static int readFile(String filename, Record[] records) throws FileNotFoundException {
		int noRecords=0;
		String buffer;
		String[] fields;
		Record record;
		
		File file = new File(filename);
		Scanner in = new Scanner(file);
		
		while(in.hasNextLine()) {
			buffer = in.nextLine();
			fields = buffer.split(",");
			record = new Record();
			record.id = Integer.parseInt(fields[0]);
			record.lastName = fields[1];
			record.firstName = fields[2];
			record.balance = Double.parseDouble(fields[3]);
			record.email = fields[4];
			records[noRecords]=record;
			noRecords++;
		}
		in.close();
		return noRecords;
	}
	
	public static void displayRecords(Record[] records, int noRecords) {
		int index;
		
		System.out.printf("%3s  %10s  %10s  %10s  %-20s\n", "ID","Last Name","First Name","Balance","Email");
		System.out.printf("%3s  %10s  %10s  %10s  %20s\n\n", "---","----------","----------","----------","--------------------");
		for(index=0;index<noRecords;index++) {
			System.out.printf("%3d) %-10s| %-10s| %10.2f| %-20s\n",records[index].id,records[index].lastName,
					records[index].firstName,records[index].balance,records[index].email);
		}
	}
	public static void readRecord(int id) {
		
	}

}
