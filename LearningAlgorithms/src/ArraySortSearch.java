import java.util.Random;
import java.util.Scanner;

public class ArraySortSearch {

	public static void main(String[] args) {
		int arrayLength;
		int search;
		int[] theArray;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("How many integers do you want in the array?");
		arrayLength = keyboard.nextInt();
		
		theArray = createRandomArray(arrayLength);
		
		mergesort(theArray);
		
//		displayArray(theArray);
		
		System.out.print("What number are we searching for? (Between 1 and 999)");
		search = keyboard.nextInt();
		
		searchArray(theArray,search);
		
		keyboard.close();
	}
	
	public static int[] mergesort(int[] theArray) {
		int index;
		int length = theArray.length;
		int middle = length/2;
		int[] left = new int[middle];
		int[] right = new int[length - middle];
		
		if(length < 2) {
			return theArray;
		}
		
		for(index=0; index<middle; index++) {
			left[index]=theArray[index];
		}
		for(index=middle; index<length; index++) {
			right[index-middle] = theArray[index];
		}
		
		mergesort(left);
		mergesort(right);
		merge(left, right, theArray);
		
		return theArray;		
	}
	
	public static int[] merge(int[] left, int[] right, int[] theArray) {
		int lenLeft = left.length;
		int lenRight = right.length;
		int rInd = 0;
		int lInd = 0;
		int index = 0;
		
		
		while (lInd < lenLeft && rInd < lenRight) {
			if(left[lInd] <= right[rInd]) {
				theArray[index] = left[lInd];
				index++;
				lInd++;
			}else {
				theArray[index] = right[rInd];
				index++;
				rInd++;
			}
		}
		while(lInd<lenLeft) {
			theArray[index] = left[lInd];
			index++;
			lInd++;
		}
		while(rInd<lenRight) {
			theArray[index] = right[rInd];
			index++;
			rInd++;
		}
		
		return theArray;
	}
	
	public static int[] createRandomArray(int arrayLength) {
		int[] theArray = new int[arrayLength];
		Random rand = new Random();
		int index;
		
		for(index=0;index<theArray.length;index++) {
			theArray[index] = rand.nextInt(999+1);
		}
		return theArray;
	}
	
	public static int[] searchArray(int[] theArray, int searching) {
		int index = 0, first, last, middle;
		
		first = 0;
		last = theArray.length-1;
		middle = (first + last) /2;
		
		while(first <= last) {
			index++;
			if(theArray[middle] < searching) {
				first = middle + 1;
			}else if(theArray[middle] == searching){
				System.out.printf("%d found at %d.\n\n", searching, middle);
				System.out.printf("It took %d passes to find.\n", index);
				break;
			}else {
				last = middle - 1;
			}
			middle = (first + last) /2;
		}
		if(first>last) {
			System.out.printf("Did not find %d in the array.", searching);
		}
		
		return theArray;
	}
	
	public static void displayArray(int[] theArray) {
		int index;
		
		for(index=0;index<theArray.length;index++) {
			System.out.printf("theArray[%2d]: %2d\n", index, theArray[index]);
		}
	}
}


